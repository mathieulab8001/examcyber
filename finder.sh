#/bin/bash

allo=1
max=200

verify=$(<"./rootPubkey.pem")
while [ $allo -le $max ]
do
	#openssl rsa -pubout -in "./q2/privateKey$allo.pem" -out "./q2/publicKey$allo.pem"
	publicFile=$(<"./q2/publicKey$allo.pem")
	if [ "$verify" = "$publicFile" ]
	then
		echo "./q2/publicKey$allo.pem"
	fi
	allo=$(( $allo + 1 ))
done
