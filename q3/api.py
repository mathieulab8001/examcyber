from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
import jwt
import sys
import datetime

ACCESS_DENIED = "Access denied."

app = Flask("EXAM_API")

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./exam.db' #sqlite:/// is the prefix to specify a relative path
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.config['BCRYPT_HANDLE_LONG_PASSWORDS'] = True
flask_bcrypt = Bcrypt(app)

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50), unique=True)
	password = db.Column(db.String(80))

def getInitialClientIP(request):
	print("trying to get IP", flush=True)
	ip = None

	if(request.data):
		if request.is_json:
			data = request.get_json()
			if 'ip' in data:
				ip = data['ip']
		else:
			print("not in json", flush=True)

	if ip == None:
		ip = request.remote_addr
	return ip



def get_user_from_token(request):
	token = None
	if 'x-access-token' in request.headers:
		token = request.headers['x-access-token']

	if not token:
		raise ValueError('Token is missing.')

	try:
		data = jwt.decode(token, app.config['SECRET_KEY'])
		user_name = data['u']

		client_ip = getInitialClientIP(request)
		token_ip = data['ip']
		if token_ip == None:
			raise ValueError('Token is not ip bound, old token now invalid.')
		elif not client_ip == token_ip:
			raise ValueError('Token is for an IP different than the client. Not valid.')


		return User.query.filter_by(name=user_name).first()
	except:
		raise ValueError('Token is invalid')

@app.route('/')
def hello():
	rep = dict()
	rep['message'] = "Welcome to the task API."

	token = None
	if 'x-access-token' in request.headers:
		token = request.headers['x-access-token']

	if not token:
		rep['user'] = 'not authenticated'
	else:
		rep['user'] = get_user_from_token(request).name

	print(str(rep), flush=True)
	return rep

@app.route('/login', methods=['post'])
def login():
	print("LOGIN route", flush=True)
	ip = getInitialClientIP(request)

	auth = request.authorization
	if not auth or not auth.username or not auth.password:
		print("LOGIN: problem with request.authorization", flush=True)
		return jsonify({'message' : 'Could not authenticate you.'})

	u = auth.username
	p = auth.password

	user = User.query.filter_by(name=u).first()
	if not user:
		print("LOGIN: No such user", flush=True)
		return jsonify({'message' : 'Could not authenticate you.'})

	if flask_bcrypt.check_password_hash(user.password, p):
		token = jwt.encode({'u':u,'ip':ip, 'exp':datetime.datetime.utcnow() + datetime.timedelta(minutes=59)}, app.config['SECRET_KEY'])

		return jsonify({'message' : 'Your are successfully authenticated. Welcome: ' + user.name, 'token':token})

	print("LOGIN: Wrong password", flush=True)
	return jsonify({'message' : 'Could not authenticate you.'})


if __name__ == '__main__':
	db.create_all() #Just need to be executed on first run to create the actual DB, but subsequent execution are harmless as far the de demo is concerned.

	if len(sys.argv) == 2:
		app.config['SECRET_KEY']=sys.argv[1]
	elif len(sys.argv) == 3:
		#We have an argument so we'll try to initialize the DB
		#TODO We could check to make sure the Admin user does not already exists...
		username = sys.argv[1]
		pwd = sys.argv[2]
		print("adding new user (" + username + ") with password (" + pwd +")")
		hashed_password = flask_bcrypt.generate_password_hash(pwd, 13)
		new_user = User(name=username, password=hashed_password)
		db.session.add(new_user)
		db.session.commit()
		sys.exit(0)

	else:
		print("Wrong number of arguments. You can either provide a single argument which is the security key for token signature, or you can provide two arguments which are respectively the username and password for a new user to be added in the DB.")
		sys.exit(1)

	app.run(debug=True, host='0.0.0.0', port=5555)
