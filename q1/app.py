from flask import Flask, request, jsonify, make_response

app = Flask("EXAM_APP")

@app.route('/')
def hello():
	return "Hello World from Exam"

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=443, ssl_context=('./fullchain.pem','./privkey.pem'))
